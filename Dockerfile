FROM python:3.9.1-buster
LABEL version="0.0.1"
LABEL maintainer="wk@tapes.wtf"

ENV TINI_VERSION="v0.19.0"

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

RUN pip install -U \
    pip \
    setuptools \
    wheel

WORKDIR /app

# Create a non-root user
RUN useradd -m -r tapeswtf && \
    chown tapeswtf /app

COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY . .

# 👇
ARG GIT_HASH
ENV GIT_HASH=${GIT_HASH:-dev}
# 👆

USER tapeswtf

ENTRYPOINT ["/tini", "--"]

CMD ["python3", "app.py"]