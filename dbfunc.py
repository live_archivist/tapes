"""
Tapes | A no bullshit bootleg collection manager
wk@wes.today
"""

import pycouchdb
import json

def load_config(cfg_file):
    with open(cfg_file) as tapes_cfg:
        tapes_config = json.load(tapes_cfg)
        return tapes_config

def db_create(server,tapes_config):
    db_name = tapes_config['db']['name']
    resp = server.create(db_name)
    return resp

def db_connect(tapes_config):
    db_user = tapes_config['db']['username']
    db_pass = tapes_config['db']['password']
    db_host = tapes_config['db']['host']
    db_port = tapes_config['db']['port']
    db_name = tapes_config['db']['name']
    db_url = "http://{0}:{1}@{2}:{3}/".format(db_user,db_pass,db_host,db_port)
    try:
        server = pycouchdb.Server(db_url)
    except:
        msg = "Could not connect to server using connection string: {}".format(db_url) 
        return msg

    try:
        db = server.database(db_name)
        return db
    except:
        db_create(server,tapes_config)
        db = server.database(db_name)
        return db 

def db_new_doc(db,payload):
    resp = db.save(payload)
    return resp