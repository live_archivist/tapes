import os
from dbfunc import *
from mediafunc import *
cfg_file = "tapes.json"
tapes_config = load_config(cfg_file)

cd = crawl_sort(tapes_config)

file_path = "./static/sort/dmb2020-02-28.flac16/dmb2020-02-28t22.flac"

metadata = get_metadata(file_path)

md5_check = md5_fingerprint(file_path)

#print(metadata)
print(md5_check)