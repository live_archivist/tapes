# Tapes

A no bullshit bootleg collection manager.

### Design Goals

- High Performance (i.e. 50TB Library managed by Raspberry Pi 4, w/ searches taking <500ms)
- Strict metadata and naming adherance to [Etree standards](http://wiki.etree.org/index.php?page=NamingStandards)
- Open REST API
- Well documented

### Modules

- Quart
- pycouchdb

### Database

CouchDB

```
docker run --name couchdb -v /home/wk/Dropbox/Projects/tapes/.database:/opt/couchdb/data -p 5984:5984 -d couchdb
```

### Docker

Running the app is very simple in docker:

```
docker run --name tapeswtf -p 3737:3737 tapeswtf:0.0.1
```

### Requirements

flac