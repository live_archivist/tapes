"""
Tapes | A no bullshit bootleg collection manager
wk@wes.today
"""

from quart import Quart, render_template, abort, Response
from dbfunc import *
from mediafunc import *
import time

cfg_file = './tapes.json'

app = Quart(__name__)

def item_lookup(item_uuid):
    print(item_uuid)
    item_payload = item_uuid
    return item_payload

@app.route('/')
async def get_home():
    """
    Home page
    ---
    get:
        description: Render home page of tapes.wtf
        responses:
            200:
                content:
                    text/html
    """
    title = "tapes[dot]wtf"
    tapes_config = load_config(cfg_file)
    db = db_connect(tapes_config)
    if db:
        db_status = "connected to db"
    else:
        db_status = "not connected to db"
    payload = {"title": title,"db": db_status}
    print(type(db))
    return await render_template('index.html', payload = payload)

@app.route('/play/<string:item_uuid>')
async def play_item(item_uuid):
    """
    Play Item
    ---
    get:
        description: Play item with uuid in route
        responses:
            200:
                content:
                    text/html
    """
    title = "tapes[dot]wtf"
    item_uuid = {item_uuid}
    item_title = ''
    item_type = ''
    playlist = ''
    payload = {"title":[],"item_uuid":[],"item_title":[],"item_type":[],"playlist":[]}

    resp = item_lookup(item_uuid)
    if resp.ok:
        if resp['title']:
            item_title = resp['title']
            payload['title'].append[item_title]
        else:
            abort(Response("No item title found"))
        
        if resp['item_type']:
            if item_type == 'album':
                print("Album")
                for t in resp['tracks']:
                    track_title = t['title']
                    track_path = t['path']
                    track_payload = ''
                    playlist
            elif item_type == 'track':
                print("Track")
            elif item_type == 'artist':
                print("Artist")
            else:
                print("idunno")
        else:
            abort(Response("No item type returned"))
        

    else:
        abort(Response("Item lookup failed"))

    # Build Payload
    
    
    payload['item_uuid'].append[item_uuid]
    payload['item_title'].append[item_title]
    payload['item_type'].append[item_type]
    payload['playlist'].append[playlist]
    # Render Page
    return await render_template('play.html', payload)

@app.route('/item/<string:item_uuid>')
async def item(item_uuid):
    print(item_uuid)

@app.route('/sort')
async def get_sort():
    """
    Home page
    ---
    get:
        description: Render home page of tapes.wtf
        responses:
            200:
                content:
                    text/html
    """
    title = "tapes.wtf | sort"
    tapes_config = load_config(cfg_file)
    db = db_connect(tapes_config)
    if db:
        db_status = "connected to db"
    else:
        db_status = "not connected to db"
    crawl = crawl_sort(tapes_config)
    crawl = crawl[0]
    show = crawl['show']
    show_path = crawl['show_path']
    show_payload = {"show": show, "show_path": show_path}
    tracks_list = []
    for t in crawl['tracks']:
        track = {"file_path": t['file_path'], "name": t['name']}
        metadata = get_metadata(t['file_path'])
        track['title'] = metadata['title'][0]
        track['artist'] = metadata['artist'][0]
        track['date'] = metadata['date'][0]
        track['show'] = metadata['album'][0]
        track['bits_per_sample'] = metadata['bits_per_sample'][0]
        track['total_samples'] = metadata['total_samples'][0]
        track['sample_rate'] = metadata['sample_rate'][0]
        track['channels'] = metadata['channels'][0]
        track['length_seconds'] = metadata['length_seconds'][0]
        track['bitrate'] = metadata['bitrate'][0]
        track['min_blocksize'] = metadata['min_blocksize'][0]
        track['max_blocksize'] = metadata['max_blocksize'][0]
        file_test = md5_test(t['file_path'])
        if file_test == 'Passed':
            track['track_health'] = file_test
        else:
            track['track_health'] = file_test
        tracks_list.append(track)
    sorted_tracks = sorted(tracks_list, key=lambda k: k['name'])
    show_payload['tracks'] = sorted_tracks
    payload = {"title": title,"db": db_status,"crawl_results": show_payload}
    print(payload)
    return await render_template('sort.html', payload = payload)

@app.route('/show')
async def get_show():
    """
    Home page
    ---
    get:
        description: Render show page of tapes.wtf
        responses:
            200:
                content:
                    text/html
    """

    
    tapes_config = load_config(cfg_file)
    db = db_connect(tapes_config)
    # if db:
    #     db_status = "connected to db"
    # else:
    #     db_status = "not connected to db"
    # show_payload = {"show_name": "", "show_date": "", "show_location": ""}
    # title = "tapes.wtf | {}".format(show_name) 
    # tracks_list = []
    # sorted_tracks = sorted(tracks_list, key=lambda k: k['name'])
    # show_payload['tracks'] = sorted_tracks
    payload = {"title": "tapes.wtf | ", "db": "connected to db", "show_artist": "Dave Matthews Band", "show": "dmb2019-06-15.flac16", "show_date": "2019-06-15", "show_name": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "show_path": "./static/sort/dmb2019-06-15.flac16", "tracks": [
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t01.flac", "name": "dmb2019-06-15t01", "title": "Introduction", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "5399016", "sample_rate": "44100", "channels": "2", "length_seconds": 122.42666666666666, "bitrate": "568253", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t02.flac", "name": "dmb2019-06-15t02", "title": "Granny", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "13267044", "sample_rate": "44100", "channels": "2", "length_seconds": 300.84, "bitrate": "728217", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t03.flac", "name": "dmb2019-06-15t03", "title": "Satellite", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "15624924", "sample_rate": "44100", "channels": "2", "length_seconds": 354.3066666666667, "bitrate": "720078", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t04.flac", "name": "dmb2019-06-15t04", "title": "Louisiana Bayou", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "24186204", "sample_rate": "44100", "channels": "2", "length_seconds": 548.44, "bitrate": "784078", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t05.flac", "name": "dmb2019-06-15t05", "title": "#27", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "12677280", "sample_rate": "44100", "channels": "2", "length_seconds": 287.46666666666664, "bitrate": "714000", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t06.flac", "name": "dmb2019-06-15t06", "title": "The Stone", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "32931528", "sample_rate": "44100", "channels": "2", "length_seconds": 746.7466666666667, "bitrate": "749228", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t07.flac", "name": "dmb2019-06-15t07", "title": "Crush", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "44811480", "sample_rate": "44100", "channels": "2", "length_seconds": 1016.1333333333333, "bitrate": "762777", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t08.flac", "name": "dmb2019-06-15t08", "title": "That Girl Is You", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "14414820", "sample_rate": "44100", "channels": "2", "length_seconds": 326.8666666666667, "bitrate": "707061", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t09.flac", "name": "dmb2019-06-15t09", "title": "What Would You Say", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "16609824", "sample_rate": "44100", "channels": "2", "length_seconds": 376.64, "bitrate": "748552", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t10.flac", "name": "dmb2019-06-15t10", "title": "Again and Again", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "14651196", "sample_rate": "44100", "channels": "2", "length_seconds": 332.2266666666667, "bitrate": "709394", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t11.flac", "name": "dmb2019-06-15t11", "title": "Sugar Will", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "18711924", "sample_rate": "44100", "channels": "2", "length_seconds": 424.3066666666667, "bitrate": "720648", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t12.flac", "name": "dmb2019-06-15t12", "title": "Sledgehammer", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "18775428", "sample_rate": "44100", "channels": "2", "length_seconds": 425.74666666666667, "bitrate": "750009", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t13.flac", "name": "dmb2019-06-15t13", "title": "The Maker", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "21794220", "sample_rate": "44100", "channels": "2", "length_seconds":  494.2, "bitrate": "698405", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t14.flac", "name": "dmb2019-06-15t14", "title": "Why I Am", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "13033020", "sample_rate": "44100", "channels": "2", "length_seconds": 295.53333333333336, "bitrate": "777877", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t15.flac", "name": "dmb2019-06-15t15", "title": "Can't Stop", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "13620432", "sample_rate": "44100", "channels": "2", "length_seconds": 308.85333333333335, "bitrate": "794052", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t16.flac", "name": "dmb2019-06-15t16", "title": "You & Me", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "14077896", "sample_rate": "44100", "channels": "2", "length_seconds": 319.2266666666667, "bitrate": "720946", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t17.flac", "name": "dmb2019-06-15t17", "title": "Jimi Thing > ", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "30891168", "sample_rate": "44100", "channels": "2", "length_seconds": 700.48, "bitrate": "771180", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t18.flac", "name": "dmb2019-06-15t18", "title": "Fly Like an Eagle", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "12489120", "sample_rate": "44100", "channels": "2", "length_seconds": 283.2, "bitrate": "666608", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t19.flac", "name": "dmb2019-06-15t19", "title": "Shake Me Like a Monkey", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "10780980", "sample_rate": "44100", "channels": "2", "length_seconds": 244.46666666666667, "bitrate": "814461", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t20.flac", "name": "dmb2019-06-15t20", "title": "Encore Break", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "21656628", "sample_rate": "44100", "channels": "2", "length_seconds": 491.08, "bitrate": "554560", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t21.flac", "name": "dmb2019-06-15t21", "title": "Tripping Billies", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "13920312", "sample_rate": "44100", "channels": "2", "length_seconds": 315.6533333333333, "bitrate": "836198", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            },
            {"file_path": "./static/sort/dmb2019-06-15.flac16/dmb2019-06-15t22.flac", "name": "dmb2019-06-15t22", "title": "All Along the Watchtower", "artist": "Dave Matthews Band", "date": "2019-06-15", "show": "Live 6.15.19 - BB&T Pavilion - Camden, NJ", "bits_per_sample": "16", "total_samples": "27446263", "sample_rate": "44100", "channels": "2", "length_seconds": 622.3642403628118, "bitrate": "726664", "min_blocksize": "4608", "max_blocksize": "4608", "track_health": "Passed"
            }
        ]
    }
    for t in payload['tracks']:
        sec = t['length_seconds']
        ty_res = time.gmtime(sec)
        res = time.strftime("%M:%S",ty_res)
        t['track_length'] = res
    return await render_template('show.html', payload = payload)

app.run(host="0.0.0.0",port="3737")