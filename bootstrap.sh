#!/bin/bash
# Install Tapes.wtf

# Check if running with sudo
if [[ "$EUID" = 0 ]]; then
    echo "[x] Running with sudo, let's proceed."
else
    sudo -k
    if sudo true; then
        echo "[x] Now running with sudo, let's proceed"
    else
        echo "(3) Wrong password"
        exit 1
    fi
fi

# Install Requirements
echo "!!!! Installing requirements for Tapes..."

if ! command -v git &> /dev/null
then
    echo "[?] git could not be found, installing"
    sudo apt-get install git -y
    echo "[x] git is now installed"
else
    echo "[x] git is already installed"
fi

if ! command -v python3 &> /dev/null
then
    echo "[?] python3 could not be found, installing"
    sudo apt-get install python3 -y
    echo "[x] python3 is now installed"
else
    echo "[x] python3 is already installed"
fi

if ! command -v pip3 &> /dev/null
then
    echo "[?] pip3 could not be found, installing"
    sudo apt-get install python3-pip -y
    echo "[x] pip3 is now installed"
else
    echo "[x] pip3 is already installed"
fi

# Create tapeswtf user and group
if id "tapeswtf" >/dev/null 2>&1; then
    echo "[x] tapeswtf user already exists"
else
    echo "[?] tapeswtf user does not exist, creating"
    sudo adduser tapeswtf --shell=/bin/false --no-create-home
    echo "[x] tapeswtf user created"
fi

if [ $(getent group tapeswtf) ]; then
    echo "[x] tapeswtf group already exists"
else
    echo "[?] tapeswtf group does not exist, creating"
    sudo groupadd tapeswtf
    echo "[x] tapeswtf group created"
fi

if id -nG "tapeswtf" | grep -qw "tapeswtf"; then
    echo "[x] tapeswtf user is in tapeswtf group"
else
    echo "[?] tapeswtf user is not in tapeswtf group, adding"
    sudo usermod -a -G tapeswtf tapeswtf
    echo "[x] tapeswtf user added to tapeswtf group"
fi

# Make working directories
if [ ! -d "/etc/tapeswtf" ]; then 
    echo "[?] /etc/tapeswtf configuration directory does not exist, creating"
    sudo mkdir /etc/tapeswtf
    echo "[x] /etc/tapeswtf configuration directory created"
else 
    echo "[x] /etc/tapeswtf configuration directory exists"
fi

if [ ! -d "/var/local/tapeswtf" ]; then
    echo "[?] /var/local/tapeswtf data directory does not exist, creating"
    sudo mkdir /var/local/tapeswtf
    echo "[x] /var/local/tapeswtf data directory created"
else
    echo "[x] /var/local/tapeswtf data directory exists"
fi

if [ ! -d "/opt/tapeswtf" ]; then
    echo "[?] /opt/tapeswtf program directory does not exist, creating"
    sudo mkdir /opt/tapeswtf
    echo "[x] /opt/tapeswtf program directory created"
else
    echo "[x] /opt/tapeswtf program directory exists"
fi

# Set Directory Permissions

etcuname="$(stat --format '%U' "/etc/tapeswtf")"
etcgroup="$(stat --format '%G' "/etc/tapeswtf")"
varuname="$(stat --format '%U' "/var/local/tapeswtf")"
vargroup="$(stat --format '%G' "/var/local/tapeswtf")"
optuname="$(stat --format '%U' "/opt/tapeswtf")"
optgroup="$(stat --format '%G' "/opt/tapeswtf")"

if [ ! "${etcuname}" = "tapeswtf" ]; then
    echo "[?] /etc/tapeswtf owner is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /etc/tapeswtf
    sudo chmod -R 664 /etc/tapeswtf
    echo "[x] /etc/tapeswtf owner is now tapeswtf"
else
    echo "[x] /etc/tapeswtf owner is already tapeswtf"
fi

if [ ! "${etcgroup}" = "tapeswtf" ]; then
    echo "[?] /etc/tapeswtf group is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /etc/tapeswtf
    sudo chmod -R 664 /etc/tapeswtf
    echo "[x] /etc/tapeswtf group is now tapeswtf"
else
    echo "[x] /etc/tapeswtf group is already tapeswtf"
fi

if [ ! "${varuname}" = "tapeswtf" ]; then
    echo "[?] /var/local/tapeswtf owner is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /var/local/tapeswtf
    sudo chmod -R 664 /var/local/tapeswtf
    echo "[x] /var/local/tapeswtf owner is now tapeswtf"
else
    echo "[x] /var/local/tapeswtf owner is already tapeswtf"
fi

if [ ! "${vargroup}" = "tapeswtf" ]; then
    echo "[?] /var/local/tapeswtf group is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /var/local/tapeswtf
    sudo chmod -R 664 /var/local/tapeswtf
    echo "[x] /var/local/tapeswtf group is now tapeswtf"
else
    echo "[x] /var/local/tapeswtf group is already tapeswtf"
fi

if [ ! "${optuname}" = "tapeswtf" ]; then
    echo "[?] /opt/tapeswtf owner is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /opt/tapeswtf
    sudo chmod -R 664 /etc/tapeswtf
    echo "[x] /opt/tapeswtf owner is now tapeswtf"
else
    echo "[x] /opt/tapeswtf owner is already tapeswtf"
fi

if [ ! "${optgroup}" = "tapeswtf" ]; then
    echo "[?] /opt/tapeswtf group is not tapeswtf, correcting"
    sudo chown -R tapeswtf:tapeswtf /opt/tapeswtf
    sudo chmod -R 664 /etc/tapeswtf
    echo "[x] /opt/tapeswtf group is now tapeswtf"
else
    echo "[x] /opt/tapeswtf group is already tapeswtf"
fi

# Clone Tapes repo
# Install Python modules
sudo pip3 install -r requirements.txt

