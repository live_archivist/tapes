"""
Tapes | A no bullshit bootleg collection manager
wk@wes.today
"""

from re import sub
from mutagen.flac import FLAC, StreamInfo
import os
import hashlib
import subprocess
import time
from subprocess import Popen, PIPE

def md5_test(file_path):
    fn,fe = os.path.splitext(file_path)
    if fe == ".flac":
        flac_fingerprint = subprocess.check_output(['metaflac', '--show-md5sum', file_path])
        flac_fingerprint = flac_fingerprint.decode("utf-8").replace("\n", "")
        flac_test = subprocess.check_output(['./shntool', 'hash', '-m', file_path]).decode("utf-8")
        print(flac_fingerprint)
        print(flac_test)
        flac_test = flac_test.split(" ")
        flac_test = flac_test[0]
        if flac_fingerprint == flac_test:
            return "Passed"
        else:
            return "Failed"

    elif fe == ".shn": 
        print("SHN File")

def get_metadata(file_path):
    """
    Get Metadata
    ---
    Collects metadata stored for FLAC and, in the future, SHN files. All you need to do is supply `file_path` and then the function will spit out something like this:
    `{'date': ['2020-02-28'], 'album': ['2020-02-28 The Chelsea at the Cosmopolitan of Las Vegas'], 'artist': ['Dave Matthews Band'], 'title': ['Ants Marching']}`
    As the metadata is expanded by Tapes, you'll find other keys in the response as well. 
    """
    fn,fe = os.path.splitext(file_path)
    if fe == ".flac":
        metadata = FLAC(file_path)
        metadata['bits_per_sample'] = str(metadata.info.bits_per_sample)
        metadata['total_samples'] = str(metadata.info.total_samples)
        metadata['sample_rate'] = str(metadata.info.sample_rate)
        metadata['channels'] = str(metadata.info.channels)
        metadata['length_seconds'] = str(metadata.info.length)
        metadata['bitrate'] = str(metadata.info.bitrate)
        metadata['min_blocksize'] = str(metadata.info.min_blocksize)
        metadata['max_blocksize'] = str(metadata.info.max_blocksize)
        return metadata
    elif fe == ".shn":
        # Gonna have to orchestrate `shorten` at the CLI
        metadata = ''
    else:
        error = {"error": "{} is not a supported filetype".format(fe)}
        return error

def get_streaminfo(file_path):
    fn,fe = os.path.splitext(file_path)
    if fe == ".flac":
        stream_info = StreamInfo(file_path)
        return stream_info
    elif fe == ".shn":
        # Gonna have to orchestrate `shorten` at the CLI
        stream_info = ''
    else:
        error = {"error": "{} is not a supported filetype".format(fe)}
        return error

def crawl_sort(tapes_config):
    """
    Crawl Sort Folder
    ---
    Function will crawl the sort folder (incoming media) and load each FLAC or SHN file into the database, all other formats will be ignored.
    Metadata will be pulled from the files, placed into a dict, sent to database for import. 
    A marker will be placed on files `import:false` which will allow it to show up in the import screen.
    """
    # list top level directories inside of sort
    sort_folder = tapes_config['collection']['sort_folder']
    crawl_directories = []
    for root, dirs, files in os.walk(sort_folder, topdown=True):
        for d in dirs:
            show_path = os.path.join(sort_folder, d)
            show = {"show": d, "show_path": show_path}
            for root,dirs,files in os.walk(show_path, topdown=True):
                tracks_list = []
                for f in files:
                    file_path = os.path.join(root, f)
                    fn,fe = os.path.splitext(f)
                    if fe == ".flac":
                        track = {"name":fn + fe, "file_path":file_path}
                        tracks_list.append(track)
                sorted_tracks = sorted(tracks_list, key=lambda k: k['name']) 
                show['tracks']=sorted_tracks
            crawl_directories.append(show) 
    return crawl_directories

